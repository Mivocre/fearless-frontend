function createCard(name, description, pictureUrl, start, end, location) {
    return `
      <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <div class="card-subtitle mb-2 text-muted">${location}</div>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${start.getMonth()}/${start.getDate()}/${start.getFullYear()} - ${end.getMonth()}/${end.getDate()}/${end.getFullYear()}</div>
      </div>
    `;
}

window.addEventListener("DOMContentLoaded", async () => {
    const url = "http://localhost:8000/api/conferences/";

    try {
        const response = await fetch(url);
        if (!response.ok) {
            console.log(`Returning code ${response.status}`);
        } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const start = new Date(details.conference.starts);
                    const end = new Date(details.conference.ends);
                    const loc = details.conference.location.name;
                    const html = createCard(
                        title,
                        description,
                        pictureUrl,
                        start,
                        end,
                        loc
                    );
                    const row = document.querySelector(".row");
                    row.innerHTML += `<div class="col pb-1">${html}</div>`;
                }
            }
        }
    } catch (e) {
        console.error(e);
    }
});
