window.addEventListener("DOMContentLoaded", async () => {
    const stateURL = "http://localhost:8000/api/states/";
    const locationUrl = "http://localhost:8000/api/locations/";
    const response = await fetch(stateURL);
    //populate stare dropdown menu
    if (response.ok) {
        const data = await response.json();
        const stateDropDown = document.querySelector("#state");
        stateDropDown.setAttribute("disabled", true);
        try {
            for (let state of data.states) {
                const option = document.createElement("option");
                option.value = state.abbreviation;
                option.innerHTML = state.name;
                stateDropDown.appendChild(option);
            }
        } catch (e) {
            console.error(e);
        }
        stateDropDown.removeAttribute("disabled");
    }
    //managing what happens when submit is clicked
    const formTag = document.getElementById("create-location-form");
    formTag.addEventListener("submit", async (e) => {
        e.preventDefault();
        const formData = new FormData(formTag);
        const formJson = JSON.stringify(Object.fromEntries(formData));
        const fetchConfig = {
            method: "post",
            body: formJson,
            headers: { "Content-Type": "application/json" },
        };
        const locationResponse = await fetch(locationUrl, fetchConfig);
        if (locationResponse.ok) {
            formTag.reset();
            const newLocation = await locationResponse.json();
        }
    });
});
