window.addEventListener("DOMContentLoaded", async () => {
    const locationURL = "http://localhost:8000/api/locations/";
    const confURL = "http://localhost:8000/api/conferences/";
    const response = await fetch(locationURL);
    //populate location dropdown menu
    if (response.ok) {
        const data = await response.json();
        const locDropDown = document.querySelector("#location");
        locDropDown.setAttribute("disabled", true);
        try {
            for (let location of data.locations) {
                const option = document.createElement("option");
                option.value = location.id;
                option.innerHTML = location.name;
                locDropDown.appendChild(option);
            }
        } catch (e) {
            console.error(e);
        }
        locDropDown.removeAttribute("disabled");
    }
    const formTag = document.getElementById("create-conference-form");
    formTag.addEventListener("submit", async (e) => {
        e.preventDefault();
        const formData = new FormData(formTag);
        console.log(formData);
        const formobj = Object.fromEntries(formData);
        formobj["description"] =
            formTag.getElementsByTagName("textarea")[0].value;
        console.log(formobj);
        const formJson = JSON.stringify(formobj);
        console.log(formJson);
        const fetchConfig = {
            method: "post",
            body: formJson,
            headers: { "Content-Type": "application/json" },
        };
        const confresponse = await fetch(confURL, fetchConfig);
        if (confresponse.ok) {
            formTag.reset();
            const newconf = await confresponse.json();
            console.log(newconf);
        }
    });
});
